use std::ops::Index;

pub trait Len: Index<usize> {
    fn leng(&self) -> usize;
}

macro_rules! impl_len {
    ( <$($( $lt:tt $($con:ident)? $( : $clt:tt $(+ $dlt:tt )* )? ),+)?> $t:ty) => {
        impl<$($( $lt $($con)? $( : $clt $(+ $dlt )* )? ),+)?> Len for $t {
            fn leng(&self) -> usize {
                self.len()
            }
        }
    };
}

impl_len!(<T>[T]);
impl_len!(<T, const N: usize>[T;N]);
impl_len!(<T>Vec<T>);
// does not work because a trait implementation could break it
//impl_len!(<T: ExactSizeIterator>T);

#[test]
fn len_test() {
    assert_eq!([12, 412, 412, 1242].leng(), 4)
}
