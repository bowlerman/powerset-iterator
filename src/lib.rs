mod length_trait;
use length_trait::Len;
use std::convert::TryInto;
use std::ops::Index;

pub struct PowersetIterator<'a, I: Index<usize> + Len  + ?Sized> {
    outer_index: u64,
    outer_max: u64,
    list_ref: &'a I,
}

pub trait IntoPowersetIterator<'a, I: Index<usize> + Len + ?Sized> {
    fn into_pow_iter(&'a self) -> PowersetIterator<'a, I>;
}

impl<'a, I: Len + ?Sized> IntoPowersetIterator<'a, I> for I {
    fn into_pow_iter(&'a self) -> PowersetIterator<'a, I> {
        PowersetIterator {
            outer_index: 0,
            outer_max: self
                .leng()
                .try_into()
                .ok()
                .and_then(|x| 2_u64.checked_shl(x))
                .unwrap_or(0), // allows iteration even if list is larger than 62
            list_ref: &self,
        }
    }
}

impl<'a, I: Len> IntoPowersetIterator<'a, I> for &I {
    fn into_pow_iter(&'a self) -> PowersetIterator<'a, I> {
        (*self).into_pow_iter()
    }
}

pub struct InnerPowersetIterator<'a, I: Index<usize> + ?Sized> {
    outer_index: u64,
    outer_max: u64,
    list_ref: &'a I,
    inner_shift: u64,
    inner_index: usize,
}

impl<'a, I: Len + ?Sized> Iterator for PowersetIterator<'a, I> {
    type Item = InnerPowersetIterator<'a, I>;
    fn next(&mut self) -> Option<InnerPowersetIterator<'a, I>> {
        let ret = (self.outer_index != self.outer_max).then(|| InnerPowersetIterator {
            outer_index: self.outer_index,
            outer_max: self.outer_max,
            list_ref: self.list_ref,
            inner_shift: 1,
            inner_index: 0,
        });
        self.outer_index += 2; // breaks if you iterate though 2^63 elements because your list has more than 62 elements
        ret
    }
}

impl<'a, I: Len + ?Sized> Iterator for InnerPowersetIterator<'a, I> {
    type Item = &'a I::Output;
    fn next(&mut self) -> Option<&'a I::Output> {
        loop {
            self.inner_shift = self.inner_shift.checked_shl(1)?;
            let shift = self.inner_shift;
            self.inner_index += 1;
            if shift >= self.outer_max {
                return None;
            } else if shift & self.outer_index == shift {
                return Some(&self.list_ref[self.inner_index - 1]);
            }
        }
    }
}

#[test]
fn test_powerset_iterator() {
    let result = vec![
        vec![],
        vec![1],
        vec![2],
        vec![1, 2],
        vec![3],
        vec![1, 3],
        vec![2, 3],
        vec![1, 2, 3],
    ];
    let x = [1_u64, 2, 3];
    let iter = (&x).into_pow_iter();
    let mut v1: Vec<Vec<u64>> = Vec::new();
    for i in iter {
        let mut v2: Vec<u64> = Vec::new();
        for j in i {
            v2.push(*j);
        }
        v1.push(v2);
    }
    assert_eq!(v1, result);
}