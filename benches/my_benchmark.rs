use criterion::{Criterion, black_box, criterion_group, criterion_main};
use powerset_iterator::IntoPowersetIterator;
use num::integer::lcm;

fn sum_of_multiples(factors: &[u128], bound: u128) -> u128 {
    let mut sum: u128 = 0;
    let mut iterator = factors.into_pow_iter();
    match iterator.next() {
        //skips first empty set
        None => return 0,
        Some(_) => {
            for i in iterator {
                let (maybe_lcm, n) = list_lcm_w_len(&mut i.map(|c| *c));
                let lcm_val: u128;
                match maybe_lcm {
                    None => return 0,
                    Some(x) => {
                        lcm_val = x;
                        let length = (bound - 1) / lcm_val;
                        let temp_sum = lcm_val * length * (length + 1) / 2;
                        match n % 2 {
                            0 => sum -= temp_sum,
                            _ => sum += temp_sum,
                        }
                    }
                }
            }
            sum
        }
    }
}

fn list_lcm_w_len<I: Iterator<Item=u128>>(mut it: I) -> (Option<u128>, usize) {
    let mut res;
    let mut n = 1;
    match it.next() {
        None => return (None, 0),
        Some(x) => {res = x.clone();}
    }
    for i in it {
        res = lcm(res, i.clone());
        n += 1;
    }
    (Some(res), n)
}

fn bench_powerset_iterator() {
    let x = [100,101,102,103,104,105,106,107,108,109,110,111];
    sum_of_multiples(black_box(&x), black_box(10_u128.pow(15)));
}

fn my_powerset_iterator_benchmark(c: &mut Criterion) {
    c.bench_function("powerset iterator", |b| b.iter(|| bench_powerset_iterator()));
}

criterion_group!(powerset_benchmarks, my_powerset_iterator_benchmark);
criterion_main!(powerset_benchmarks);